import styles from "./product-list-item.module.scss";

const ProductListItem = (props) => {
  return (
    <div className={styles.content}>
      <div>
        <img src={props.item.image} alt={props.item.title} style={{ width: "100%" }} />
      </div>
      <div>{props.item.title}</div>
      <div className={styles.price}><span>{'£'}</span>{props.item.variantPriceRange.value.min}</div>
    </div>
  );
};

export default ProductListItem;
