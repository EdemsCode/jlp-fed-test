import Head from "next/head";
import Link from "next/link";
import styles from "./index.module.scss";
import useSWR from 'swr'
import ProductListItem from "../components/product-list-item/product-list-item";

const Home = () => {
 const fetcher = (...args) => fetch(...args).then((res) => res.json())
 const { data, error } = useSWR("https://api.johnlewis.com/search/api/rest/v2/catalog/products/search/keyword?q=dishwasher&key=AIzaSyDD_6O5gUgC4tRW5f9kxC0_76XRC8W7_mI"
 , fetcher)
 const numberOfItemsToBeDisplayed = 20;
 let items = data?.products;
 
  return (
    <div>
      <Head>
        <title>JL &amp; Partners | Home</title>
        <meta name="keywords" content="shopping" />
      </Head>
      <div>
        <h1>Dishwashers ({numberOfItemsToBeDisplayed})</h1>
        <div className={styles.content}>
          {items?.slice(0, numberOfItemsToBeDisplayed).map((item) => (
            <Link
              key={item.productId}
              href={{
                pathname: "/product-detail/[id]",
                query: { id: item.productId },
              }}
            >
              <a className={styles.link}>
                <ProductListItem item={item}/>
              </a>
            </Link>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Home;
