import {useRouter} from 'next/router';
import useSWR from 'swr';
import ProductCarousel from "../../components/product-carousel/product-carousel";


const ProductDetail = () => {
  const router = useRouter()
  const { id } = router.query
  const fetcher = (...args) => fetch(...args).then((res) => res.json())
  const { data, error } = useSWR("https://api.johnlewis.com/mobile-apps/api/v1/products/" + id 
  , fetcher)
  return (
    <div>
      <h1>{ data?.title }</h1>
      <div>

        <ProductCarousel image={data?.media.images.urls[0]} />

        <h3>
          <h1>{data?.price.now}</h1>
          <div>{data?.displaySpecialOffer}</div>
          <div>{data?.additionalServices.includedServices}</div>
        </h3>

        <div>
          <h3>Product information</h3>
        </div>
        <h3>Product specification</h3>
        <ul>
          {data?.details.features[0].attributes.map((item) => (
            <li key={item.productId}>
              <div>{ item.name }</div>
            </li>
          ))}
        </ul>
      </div> 
    </div>
  );
};

export default ProductDetail;
